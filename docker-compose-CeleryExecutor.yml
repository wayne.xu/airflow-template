version: '2.2'
services:
    redis:
        image: 'redis:5.0.5'
        # command: redis-server --requirepass redispass
        container_name: celery-broker-redis
        ports:
            - "${REDIS_PORT}:6379"

    postgres:
        image: postgres:9.6
        container_name: airflow-meta-db
        environment:
            - POSTGRES_USER=${META_DB_USER}
            - POSTGRES_PASSWORD=${META_DB_PASSWORD}
            - POSTGRES_DB=${META_DB_NAME}
        # Uncomment these lines to persist data on the local filesystem.
        #     - PGDATA=/var/lib/postgresql/data/pgdata
        # volumes:
        #     - ./pgdata:/var/lib/postgresql/data/pgdata
        ports:
            - "${META_DB_PORT}:5432"

    webserver:
        build:
            context: .
            dockerfile: Dockerfile
        container_name: airflow-webserver
        restart: always
        depends_on:
            - postgres
            - redis
        environment:
            - LOAD_EX=n
            - FERNET_KEY=${FERNET_KEY}
            - EXECUTOR=Celery
            - DEPLOYMENT_ENV=${DEPLOYMENT_ENV}
            - AIRFLOW__CORE__SQL_ALCHEMY_CONN=${META_DB_PREFIX}://${META_DB_USER}:${META_DB_PASSWORD}@${META_DB_CONTAINER_NAME}:${META_DB_PORT}/${META_DB_NAME}
            - AIRFLOW__CELERY__RESULT_BACKEND=${CELERY_RESULT_BACKEND_PREFIX}://${META_DB_USER}:${META_DB_PASSWORD}@${META_DB_CONTAINER_NAME}:${META_DB_PORT}/${META_DB_NAME}
            # Add work directory, if you want to import module which doesn't contain in dags and plugins folders.
            # - PYTHONPATH=/usr/local/airflow/:$PYTHONPATH
        volumes:
            - ./config:/usr/local/airflow/config
            - ./dags:/usr/local/airflow/dags
            - ./plugins:/usr/local/airflow/plugins
            - ./requirements.txt:/requirements.txt
        ports:
            - "${WEBSERVER_PORT}:8080"
        command: webserver
        healthcheck:
            test: ["CMD-SHELL", "[ -f /usr/local/airflow/airflow-webserver.pid ]"]
            interval: 30s
            timeout: 30s
            retries: 3

    flower:
        build:
            context: .
            dockerfile: Dockerfile
        container_name: celery-flower
        restart: always
        depends_on:
            - redis
        environment:
            - EXECUTOR=Celery
            # - REDIS_PASSWORD=redispass
        ports:
            - "${FLOWER_PORT}:5555"
        command: celery flower

    scheduler:
        build:
            context: .
            dockerfile: Dockerfile
        container_name: airflow-scheduler
        restart: always
        depends_on:
            - webserver
        volumes:
            - ./config:/usr/local/airflow/config
            - ./dags:/usr/local/airflow/dags
            - ./plugins:/usr/local/airflow/plugins
            - ./requirements.txt:/requirements.txt
        environment:
            - LOAD_EX=n
            - FERNET_KEY=${FERNET_KEY}
            - EXECUTOR=Celery
            - DEPLOYMENT_ENV=${DEPLOYMENT_ENV}
            - AIRFLOW__CORE__SQL_ALCHEMY_CONN=${META_DB_PREFIX}://${META_DB_USER}:${META_DB_PASSWORD}@${META_DB_CONTAINER_NAME}:${META_DB_PORT}/${META_DB_NAME}
            - AIRFLOW__CELERY__RESULT_BACKEND=${CELERY_RESULT_BACKEND_PREFIX}://${META_DB_USER}:${META_DB_PASSWORD}@${META_DB_CONTAINER_NAME}:${META_DB_PORT}/${META_DB_NAME}
            # Add work directory, if you want to import module which doesn't contain in dags and plugins folders.
            # - PYTHONPATH=/usr/local/airflow/:$PYTHONPATH
        command: scheduler

    worker:
        build:
            context: .
            dockerfile: Dockerfile
        container_name: airflow-worker
        restart: always
        depends_on:
            - scheduler
        volumes:
            - ./config:/usr/local/airflow/config
            - ./dags:/usr/local/airflow/dags
            - ./plugins:/usr/local/airflow/plugins
            - ./requirements.txt:/requirements.txt
        environment:
            - FERNET_KEY=${FERNET_KEY}
            - EXECUTOR=Celery
            - DEPLOYMENT_ENV=${DEPLOYMENT_ENV}
            - AIRFLOW__CORE__SQL_ALCHEMY_CONN=${META_DB_PREFIX}://${META_DB_USER}:${META_DB_PASSWORD}@${META_DB_CONTAINER_NAME}:${META_DB_PORT}/${META_DB_NAME}
            - AIRFLOW__CELERY__RESULT_BACKEND=${CELERY_RESULT_BACKEND_PREFIX}://${META_DB_USER}:${META_DB_PASSWORD}@${META_DB_CONTAINER_NAME}:${META_DB_PORT}/${META_DB_NAME}
            # Add work directory, if you want to import module which doesn't contain in dags and plugins folders.
            # - PYTHONPATH=/usr/local/airflow/:$PYTHONPATH
        command: celery worker
