# Airflow 2.0.0 Docker-Compose

## Informations

 - Based on Python(3.8-slim-buster) official image and use the official [Postgres](https://www.postgresql.org/) as backend db and [Redis](https://redis.io/) as queue.

## Build 

### There are two types of executor, 

  - CeleryExecutor:
    ```shell
    docker-compose --env-file=./config/.env -f docker-compose-CeleryExecutor.yml up -d
    ```
    or
    ```shell
    bash run-server.sh
    ```

## Custom Airflow plugins

  - Create the plugins folders plugins/ with your custom plugins.
  - Mount the folder as a volume by doing either of the following:
    - Include the folder as a volume in command-line ```-v $(pwd)/plugins/:/usr/local/airflow/plugins```

## Install custom python package

  - Create a file "requirements.txt" with the desired python modules
  - Mount this file as a volume ```-v $(pwd)/requirements.txt:/requirements.txt``` (or add it as a volume in docker-compose file)
  - The entrypoint.sh script execute the pip install command (with --user option)