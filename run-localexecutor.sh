#! /bin/bash

DKC_VERSION=$(docker-compose --version)
DKC_FILE_NAME='docker-compose-LocalExecutor.yml'
ENV_FILE_PATH="./config/.env"

echo "Host machine ${DKC_VERSION}"
echo "docker-compose env-file => ${ENV_FILE_PATH}"

docker-compose --env-file=${ENV_FILE_PATH} -f ${DKC_FILE_NAME} down
docker-compose --env-file=${ENV_FILE_PATH} -f ${DKC_FILE_NAME} up -d
