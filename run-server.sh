#!/bin/bash

read -p "Are you sure to start docker-compose? (Input YES or NO) " START_OR_NOT

if [ "${START_OR_NOT}" == "YES" ] || [ "${START_OR_NOT}" == "yes" ]; then
    
    # Determine how to add connection information to metadb.
    read -p "Do you want to add connection information from configuration? (Input YES or NO) " DKC_ADD_CONN_INFO
    read -p "Do you want to import variables from configuration? (Input YES or NO) " IMPORT_VARIABLES

    DKC_VERSION_BASELINE=1.25
    HOST_DKC_VERSION=$(docker-compose --version | cut -d " " -f3 | cut -d "." -f 1,2)
    HOST_FULL_DKC_VERSION=$(docker-compose --version | cut -d ' ' -f3 | cut -d "," -f 1)

    # Check docker-compose version.
    if (( ${DKC_VERSION_BASELINE%%.*} > ${HOST_DKC_VERSION%%.*} ||\
        ( ${DKC_VERSION_BASELINE%%.*} == ${HOST_DKC_VERSION%%.*} && ${DKC_VERSION_BASELINE##*.} > ${HOST_DKC_VERSION##*.} ) )); then
        echo "[Error]"
        echo "Your machine docker-compose version must be higher than ${DKC_VERSION_BASELINE}"
        echo "Your machine docker-compose version is ${HOST_FULL_DKC_VERSION}"
        exit 1
    fi

    DKC_FILE_NAME="docker-compose-CeleryExecutor.yml"
    ENV_FILE_PATH="./config/.env"

    echo "Host machine docker-compose version is ${HOST_FULL_DKC_VERSION}"
    echo "docker-compose env file path => ${ENV_FILE_PATH}"

    docker-compose --env-file=${ENV_FILE_PATH} -f ${DKC_FILE_NAME} down
    docker-compose --env-file=${ENV_FILE_PATH} -f ${DKC_FILE_NAME} up -d

    wait_for_webserver() {
        # Setting check container variables.
        # It costs almost 1 minutes for waiting airflow-webserver to be healthy. 
        local CHECK_TIMES=0
        local CHECK_MAX_TIMES=5
        local CHECK_TIME_INTERVAL=20
        local TOTAL_TIME=0
        local NUM_HEALTHY_CONTAINER=0
        local CONTAINER_NAME=$1

        while true
        do
            CHECK_TIMES=$(( CHECK_TIMES+1 ))
            NUM_HEALTHY_CONTAINER=$( docker ps --filter name=${CONTAINER_NAME} --filter health=healthy | grep ${CONTAINER_NAME} | wc -l )

            if [ ${NUM_HEALTHY_CONTAINER} -eq 1 ]; then
                echo "container_name: ${CONTAINER_NAME} is healthy now. Totally wait ${TOTAL_TIME} seconds."
                break
            elif [ ${CHECK_TIMES} -eq $(( ${CHECK_MAX_TIMES}+1 )) ]; then
                echo "[ERROR]"
                echo "container_name: ${CONTAINER_NAME} occurs some problems."
                echo "Please check it again."
                exit 1
            fi
            # If it isn't healthy, it will check it again until reaching stopping criteria.
            if [ ${NUM_HEALTHY_CONTAINER} -ne 1 ] && [ ${CHECK_TIMES} -lt $(( ${CHECK_MAX_TIMES}+1 )) ]; then
                TOTAL_TIME=$(( ${TOTAL_TIME} + ${CHECK_TIME_INTERVAL} ))
                printf "Check container times: ${CHECK_TIMES}, and wait for ${CONTAINER_NAME} to be healthy, "
                echo "totally wait ${TOTAL_TIME} seconds"
                sleep ${CHECK_TIME_INTERVAL}
            fi
        done
    }

    # Add connection information to Airflow meta database.
    add_conn_info() {
        # It should be the same as the container name.
        local CONNECTION_FILE_PATH=$1
        local CONTAINER_NAME=$2
    
        RESULT=$( docker exec -it ${CONTAINER_NAME} bash ${CONNECTION_FILE_PATH} )
        printf "${RESULT}"
    }

    # Add variables to Airflow meta database.
    add_variables() {
        # It should be the same as the container name.
        local IMPORT_FILE_PATH=$1
        local CONTAINER_NAME=$2

        ADD_VARIABLE_CLI="airflow variables import ${IMPORT_FILE_PATH}"
        RESULT=$( docker exec -it ${CONTAINER_NAME} ${ADD_VARIABLE_CLI} )
        printf "${RESULT}"
    }

    # Check weather use docker-compose environment to add connection information.
    if [ "${DKC_ADD_CONN_INFO}" == "YES" ] || [ "${DKC_ADD_CONN_INFO}" == "yes" ]; then
        
        CONNECTION_FILE_PATH="./config/conn_cli.sh"
        if [ -f "${CONNECTION_FILE_PATH}" ]; then
            CONTAINER_NAME="airflow-webserver"
            wait_for_webserver ${CONTAINER_NAME}
            add_conn_info ${CONNECTION_FILE_PATH} ${CONTAINER_NAME}
        else
            echo "[Error] file: ${CONNECTION_FILE_PATH} doesn't exits."
        fi
    fi

    if [ "${IMPORT_VARIABLES}" == "YES" ] || [ "${IMPORT_VARIABLES}" == "yes" ]; then
        
        IMPORT_FILE_PATH="./config/variables.json"
        if [ -f "${CONNECTION_FILE_PATH}" ]; then
            CONTAINER_NAME="airflow-webserver"
            wait_for_webserver ${CONTAINER_NAME}
            add_variables ${IMPORT_FILE_PATH} ${CONTAINER_NAME}
        else
            echo "[Error] file: ${IMPORT_FILE_PATH} doesn't exits."
        fi
    fi

    echo "Finish docker-compose !!!"

else
    echo "Stop docker-compose......"
fi